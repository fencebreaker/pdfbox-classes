name := "pdfbox-classes"

version := "1.0"

scalaVersion := "2.12.4"

libraryDependencies ++= Seq(
  "org.apache.pdfbox" % "pdfbox" % "2.0.4"
)
