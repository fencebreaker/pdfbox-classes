package pdftotext

import java.io.{ByteArrayInputStream, InputStream}

import org.apache.pdfbox.pdmodel.font.{PDFont, PDFontFactory}
import org.apache.pdfbox.pdmodel.graphics.state.{PDGraphicsState, PDTextState}
import org.apache.pdfbox.util.{Matrix, Vector}

/**
  * PdfTextStripper that uses the customized GlyphList in resources/glyphlist/additional.txt
  */
class PDFTextStripperWithGlyphs(val paragraphStart: Char, val pageStart: Char) extends ModifiedClassesFromPdfbox.PDFTextStripper {

  setParagraphStart(paragraphStart.toString)
  setPageStart(pageStart.toString)

  /**
    * Version of showText that uses the customized GlyphList in
    * resources/glyphlist/additional.txt
    *
    * @param string text
    */
  override def showText (string: Array[Byte]): Unit = {
    val state: PDGraphicsState = getGraphicsState
    val textState: PDTextState = state.getTextState
    var font: PDFont = textState.getFont
    if (font == null) {
      //LOG.warn("No current font, will use default")
      font = PDFontFactory.createDefaultFont
    }
    val fontSize: Float = textState.getFontSize
    val horizontalScaling: Float = textState.getHorizontalScaling / 100f
    val charSpacing: Float = textState.getCharacterSpacing
    val parameters: Matrix = new Matrix(fontSize * horizontalScaling, 0, 0, fontSize, 0, textState.getRise)
    val in: InputStream = new ByteArrayInputStream(string)
    while (in.available > 0) {
      val textMatrix = getTextMatrix
      val textLineMatrix = getTextLineMatrix


      val before: Int = in.available
      val code: Int = font.readCode(in)
      val codeLength: Int = before - in.available
      val unicode: String = font.toUnicode(code, this.glyphList) //modified code line
      var wordSpacing: Float = 0
      if (codeLength == 1 && code == 32) {
        wordSpacing += textState.getWordSpacing
      }
      val ctm: Matrix = state.getCurrentTransformationMatrix
      val textRenderingMatrix: Matrix = parameters.multiply(textMatrix).multiply(ctm)
      if (font.isVertical) {
        val v: Vector = font.getPositionVector(code)
        textRenderingMatrix.translate(v)
      }
      val w: Vector = font.getDisplacement(code)
      saveGraphicsState()
      val textMatrixOld: Matrix = textMatrix
      val textLineMatrixOld: Matrix = textLineMatrix
      showGlyph(textRenderingMatrix, font, code, unicode, w)
      setTextMatrix(textMatrixOld)
      setTextLineMatrix(textLineMatrixOld)
      restoreGraphicsState()
      var tx: Float = .0f
      var ty: Float = .0f
      if (font.isVertical) {
        tx = 0
        ty = w.getY * fontSize + charSpacing + wordSpacing
      }
      else {
        tx = (w.getX * fontSize + charSpacing + wordSpacing) * horizontalScaling
        ty = 0
      }
      textMatrix.concatenate(Matrix.getTranslateInstance(tx, ty))
    }
  }

}
