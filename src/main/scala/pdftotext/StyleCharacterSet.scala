package pdftotext

import java.util.regex.Pattern

object StyleCharacterSet {

  /**
    * default StyleCharacterSet with style characters that won't occur in PONS
    */
  val default: StyleCharacterSet = StyleCharacterSet(
    paragraphStart   = '¶',
    pageStart        = '↭',
    startItalic      = '↬',
    endItalic        = '↫',
    startBold        = '↪',
    endBold          = '↩',
    startBlue        = '⇉',
    endBlue          = '⇇',
    unknownPhonetics = '☹'
  )
}

/**
  * A set of characters for styling information
  *
  * The styling information shall be used for text extracted from PONS
  *
  * @param startItalic string indicating the start of italic text
  * @param endItalic string indicating the end of italic text
  * @param startBold string indicating the start of bold text
  * @param endBold string indicating the end of bold text
  * @param startBlue string indicating the start of blue text
  * @param endBlue string indicating the end of blue text
  */
case class StyleCharacterSet( paragraphStart: Char,
                              pageStart: Char,
                              startItalic: Char,
                              endItalic: Char,
                              startBold: Char,
                              endBold: Char,
                              startBlue: Char,
                              endBlue: Char,
                              unknownPhonetics: Char
                            ) {


  /**
    * Pairs of opening and closing Style-Characters
    */
  val styleCharacterPairs: List[(Char, Char)] = List((startBlue, endBlue), (startItalic, endItalic), (startBold, endBold))

  /**
    * List of pairs of brackets relevant in the text
    */
  val importantBrackets: List[(Char, Char)] = List(('(', ')'), ('{', '}'), ('[', ']'), ('<', '>'))

  /**
    * Regexp for opening and immediately closing Style-Characters that can be removed
    */
  val removableStyleCharacterPattern: Pattern = Pattern.compile("(" + styleCharacterPairs.map(pair => pair._1 + pair._2.toString).mkString("|") + ")")

  /**
    * Method to add missing opening and closing style characters
    * @param content content to correct
    * @return corrected content
    */
  def insertMissingOpeningAndClosingSC(content: String): String = {
    var text:String = content


    //Insert missing opening Style-Characters
    styleCharacterPairs.foreach{
      case (start, end) =>
        if (text.contains(end) && (!text.contains(start) || text.indexOf(end) < text.indexOf(start)))
          text = start + text
    }

    //Insert missing closing Style-Characters
    styleCharacterPairs.foreach{
      case (start, end) =>
        if(text.contains(start) && (!text.contains(end) || text.lastIndexOf(end) < text.lastIndexOf(start)))
          text = text + end
    }

    text
  }

  /**
    * Remove style charactes that do not format text
    * @param text text to work on
    * @return text without style charaters that open and close immediatly
    */
  def removeUnnecessaryStyleCharacterPattern(text: String) : String =
    removableStyleCharacterPattern.matcher(text).replaceAll("")

  /**
    * Method that removes style characters from within blue text
    * @param text text to work on
    * @return text without style characters within the blue parts of the text
    */
  def removeStyleCharactersInsideOfBlueText(text: String): String = {
    var insideBlue:Boolean = false

    var newStr:List[Char] = Nil

    for (i <- text.length - 1 to 0 by -1) {
      val c:Char = text.apply(i)

      newStr = c::newStr

      if (c == endBlue) {
        insideBlue = true
      } else if (c == startBlue) {
        insideBlue = false
      } else if (isStyleCharacter(c) && insideBlue) {
        //Removes SC inside blue Text
        newStr = newStr.tail
      }
    }

    newStr.mkString
  }

  /**
    * Orders the Style-Characters in a given content-string and inserts opening and closing Characters
    *
    * @param content The input-String
    * @return The String with ordered Style-Characters and opening and closing Style-Characters
    */
  def orderStyleCharactersInString(content: String): String = {
    var text:String = insertMissingOpeningAndClosingSC(content)

    var testText:String = ""
    //Fixpunkt notwendig??. Verbesserungsvoschläge erwünscht
    do {
      testText = text

      //Opening Style-Characters move closer to the closing ones
      text = orderStyleCharacter(priorityOnOpen = true, text)

      //Closing Style-Characters move closer to the opening ones
      text = text.reverse
      text = orderStyleCharacter(priorityOnOpen = false, text)
      text = text.reverse

      //Remove unimportant Style-Character-Pairs
      text = removeUnnecessaryStyleCharacterPattern(text)
    } while(text != testText)

    text = removeStyleCharactersInsideOfBlueText(text)

    text
  }


  /**
    * Orders the Style-Characters in a given str-string
    * @param priorityOnOpen True IFF the opening Style-Characters should be ordered
    * @param str The input-String
    * @return The String with ordered Style-Characters
    */
  def orderStyleCharacter(priorityOnOpen:Boolean, str:String) : String = {
    val keyValues: Map[Char, Char] = if (priorityOnOpen) styleCharacterPairs.toMap else styleCharacterPairs.map(_.swap).toMap

    val bracketsSameDir:List[Char] = if (priorityOnOpen) importantBrackets.map(_._1) else importantBrackets.map(_._2)
    val bracketsOtherDir:List[Char] = if (priorityOnOpen) importantBrackets.map(_._2) else importantBrackets.map(_._1)


    if (str.length() == 0)
      return str

    var newStr:List[Char] = Nil
    var xs:List[Char] = str.toList

    for (i <- 0 until str.length()) {
      if (i == str.length() - 1) {
        newStr = xs.head :: newStr
      } else {
        val c:Char = xs.head
        val d:Char = xs.tail.head
        if (keyValues.contains(c) &&
          (keyValues.contains(d) && xs.indexOf(keyValues(c)) < xs.indexOf(keyValues(d))
            || d == paragraphStart || d.isControl  || d.isSpaceChar || bracketsOtherDir.contains(d))) {
          newStr = d :: newStr

          //Tauscht die Reihenfolge der ersten zwei Elemente in der Liste
          xs = d :: c :: xs.tail.tail
        } else if (bracketsSameDir.contains(c) && keyValues.contains(d)) {
          newStr = d :: newStr

          //Tauscht die Reihenfolge der ersten zwei Elemente in der Liste
          xs = d :: c :: xs.tail.tail
        } else {
          newStr = c :: newStr
        }
      }

      xs = xs.tail
    }

    newStr.reverse.mkString
  }


  /**
    * @return List of all style charaters from this StyleCharacterSet
    */
  def styleCharacters: List[Char] = List(
    paragraphStart,
    pageStart,
    startItalic,
    startBold,
    startBlue,
    endItalic,
    endBold,
    endBlue
  )


  /**
    * Method to calculate whether the end of a text is formatted in a certain way
    * @param start start character for the formatting that is examined
    * @param end end character for the formatting that is examined
    * @param str text to examine
    * @return True iff the end of th text is fomated using start and end
    */
  def hasFormattedEnd(start: Char, end: Char)(str: String): Boolean =
    str.filter{c => c == start || c == end}.contains(start)

  /**
    * Method to calculate whether the end of a text is blue
    * @return True iff the end of th text is blue
    */
  def hasBlueEnd: String => Boolean = hasFormattedEnd(startBlue, endBlue)

  /**
    * Method to calculate whether the end of a text is bold
    * @return True iff the end of th text is bold
    */
  def hasBoldEnd: String => Boolean = hasFormattedEnd(startBold, endBold)

  /**
    * Method to calculate whether the end of a text is italic
    * @return True iff the end of th text is italic
    */
  def hasItalicEnd: String => Boolean = hasFormattedEnd(startItalic, endItalic)

  /**
    * Methode to remove all style characters from a string and trim it
    * afterwards
    * @param str string to remove style characters from
    * @return string without style characters
    */
  def removeStyleCharactersAndTrim(str: String): String =
    str.filterNot{styleCharacters.contains}.trim

  /**
    * Methode to remove all style characters from a string
    * @param str string to remove style characters from
    * @return string without style characters
    */
  def removeStyleCharacters(str: String): String =
    str.filterNot{styleCharacters.contains}

  /**
    * Method that returns true if and only if a character is a style character
    * @param c input character
    * @return true if and only if a character is a style character
    */
  def isStyleCharacter(c: Char): Boolean = styleCharacters.mkString("").contains(c)

  /**
    * Patterns describing common mistakes in word entries of PONS
    */
  lazy val commonMistakes: List[(Pattern, String)] = List(
    //Pattern.compile(s"\\[.*$unknownPhonetics.*\\]") -> "",
    //Pattern.compile(s"<.*>") -> "",
    Pattern.compile(s" $endItalic\\)")              -> s"$endItalic)",
    Pattern.compile(s" $endBold\\)")                -> s"$endBold)",
    Pattern.compile(" \\)")                         -> ")"
  )

  /**
    * Method to correct the most common mistakes in word entries from PONS
    * (like blanks in front of closing parentheses)
    */
  def correctCommonMistakes(input: String): String = {
    var string: String = input

    commonMistakes.foreach{case (pattern, replacement) =>
      string = pattern.split(string).mkString(replacement)
    }

    string
  }
}