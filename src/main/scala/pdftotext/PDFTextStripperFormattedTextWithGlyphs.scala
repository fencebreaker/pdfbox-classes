package pdftotext

import ModifiedClassesFromPdfbox.PDFTextStripper
import org.apache.pdfbox.contentstream.operator.color._
import org.apache.pdfbox.pdmodel.PDPage
import org.apache.pdfbox.pdmodel.graphics.color.PDColor
import org.apache.pdfbox.text.TextPosition

import scala.collection.convert.ImplicitConversionsToScala._
import scala.collection.mutable

object PDFTextStripperFormattedTextWithGlyphs {

  def apply(): PDFTextStripperFormattedTextWithGlyphs =
    new PDFTextStripperFormattedTextWithGlyphs(StyleCharacterSet.default)

  /**
    * Returns true iff a PDColor is blue
    * @param color color to examine
    * @return true iff color is blue
    */
  def isBlue(color: PDColor): Boolean =
    color.getComponents.headOption.contains(1.0)

  /**
    * Returns blue if a TextPosition is within a part of the text that ist formatted italic
    * @param textPosition TextPosition to examine
    * @return true iff textPosition is within a text that is formatted italic
    */
  def isItalic(textPosition: TextPosition): Boolean =
    textPosition.getFont.getFontDescriptor.isItalic

  /**
    * Returns blue if a TextPosition is within a part of the text that ist formatted bold
    * @param textPosition TextPosition to examine
    * @return true iff textPosition is within a text that is formatted bold
    */
  def isBold(textPosition: TextPosition): Boolean =
    textPosition.getFont.getFontDescriptor.isForceBold

}

/**
  * A TextStripper that keeps information about the text's format by adding
  * strings that indicate the start and end of italic, bold and blue text
  *
  * This TextStripper also uses the customized GlyphList in
  * resources/glyphlist/additional.txt
  */
class PDFTextStripperFormattedTextWithGlyphs(val styleCharacterSet: StyleCharacterSet)
  extends PDFTextStripperWithGlyphs(paragraphStart =  styleCharacterSet.paragraphStart, pageStart = styleCharacterSet.pageStart) {

  import PDFTextStripperFormattedTextWithGlyphs._

  //----------------------------------------------------------------------------
  //---------------------- Deal with blue text ---------------------------------
  //----------------------------------------------------------------------------

  addOperator(new SetStrokingColorSpace)
  addOperator(new SetNonStrokingColorSpace)
  addOperator(new SetStrokingDeviceCMYKColor)
  addOperator(new SetNonStrokingDeviceCMYKColor)
  addOperator(new SetNonStrokingDeviceRGBColor)
  addOperator(new SetStrokingDeviceRGBColor)
  addOperator(new SetNonStrokingDeviceGrayColor)
  addOperator(new SetStrokingDeviceGrayColor)
  addOperator(new SetStrokingColor)
  addOperator(new SetStrokingColorN)
  addOperator(new SetNonStrokingColor)
  addOperator(new SetNonStrokingColorN)

  /**
    * Set of TextPositions with blue text
    */
  val blueTextPositions: mutable.Set[TextPosition] = mutable.Set.empty[TextPosition]

  //Collects all blue text positions in blueTextPositions
  override def processTextPosition(text: TextPosition): Unit = {
    super.processTextPosition(text)

    val color: PDColor = getGraphicsState.getNonStrokingColor
    if (PDFTextStripperFormattedTextWithGlyphs.isBlue(color)) blueTextPositions.add(text)

  }

  override def processPage(page: PDPage): Unit = {
    super.processPage(page)
    blueTextPositions.clear()
    resetStyle()
  }

  private def normalizeAdd(
                            normalized: java.util.List[PDFTextStripper.WordWithTextPositions],
                            lineBuilder: java.lang.StringBuilder,
                            wordPositions: java.util.List[TextPosition],
                            item: PDFTextStripper.LineItem,
                            styleChanged: Boolean
                          ): java.lang.StringBuilder = {

    val currentLineBuilder = if(styleChanged || item.isWordSeparator) {
      if(lineBuilder.toString.nonEmpty) {
        normalized.add(createWord(lineBuilder.toString, new java.util.ArrayList[TextPosition](wordPositions)))
      }
      wordPositions.clear()
      new java.lang.StringBuilder
    } else {
      lineBuilder
    }


    if (!item.isWordSeparator) {
      val text: TextPosition = item.getTextPosition
      currentLineBuilder.append(text.getUnicode)
      wordPositions.add(text)
    }
    currentLineBuilder
  }

  override def normalize(line: java.util.List[PDFTextStripper.LineItem]): java.util.List[PDFTextStripper.WordWithTextPositions] = {
    val normalized: java.util.List[PDFTextStripper.WordWithTextPositions] =
      new java.util.LinkedList[PDFTextStripper.WordWithTextPositions]

    var lineBuilder: java.lang.StringBuilder = new java.lang.StringBuilder
    val wordPositions: java.util.List[TextPosition] = new java.util.ArrayList[TextPosition]

    var oldStyle: Boolean = false

    for (item <- line) {
      val tp = item.getTextPosition
      val newStyle = isBlue(tp)
      lineBuilder = normalizeAdd(normalized, lineBuilder, wordPositions, item, newStyle != oldStyle)
      oldStyle = newStyle
    }
    if (lineBuilder.length > 0) {
      normalized.add(createWord(lineBuilder.toString, wordPositions))
    }
    return normalized
  }

  //----------------------------------------------------------------------------
  //---------------- Add font information to the output ------------------------
  //----------------------------------------------------------------------------

  /**
    * Variable to store the style information for the current text position
    */
  var italicBoldBlue: (Boolean, Boolean, Boolean) = (false, false, false)

  /**
    * reset to current stlye to not italic, not bold and not blue
    */
  def resetStyle(): Unit = italicBoldBlue = (false, false, false)

  /**
    * Method to find out whether a text position in blue
    * @param textPosition TextPosition to examine
    * @return true iff the text of this TextPosition is printed in blue
    */
  def isBlue(textPosition: TextPosition): Boolean =
    blueTextPositions.contains(textPosition)

  override def writeString(text: String, textPositions: java.util.List[TextPosition]) = {

    var str: String = ""

    //textPositions.lastOption.foreach{ tp =>
    textPositions.foreach{ tp =>
      val newStyle: (Boolean, Boolean, Boolean) = (isItalic(tp), isBold(tp), isBlue(tp))

      //close style blocks
      if(italicBoldBlue._3 && ! newStyle._3) str += styleCharacterSet.endBlue
      if(italicBoldBlue._2 && ! newStyle._2) str += styleCharacterSet.endBold
      if(italicBoldBlue._1 && ! newStyle._1) str += styleCharacterSet.endItalic

      //open style blocks
      if(! italicBoldBlue._1 && newStyle._1) str += styleCharacterSet.startItalic
      if(! italicBoldBlue._2 && newStyle._2) str += styleCharacterSet.startBold
      if(! italicBoldBlue._3 && newStyle._3) str += styleCharacterSet.startBlue


      italicBoldBlue = newStyle
    }

    //writeString(str + s"{$text}")
    writeString(str + text)
  }
}